# PowerNap
Sleep library for ATtiny85.

This is my attempt to make a small and simple sleep library for my ATTiny85 projects.

## Power consumption
The power consumption in Sentry mode is 0.12mAh measured on a cheap multimeter :)
The power consumption in Sleep mode is 0.003mAh measured on the same multimeter.

## Delay
This library provides `napper.delay()` for use when the normal delay cannot be used.
This is the case when you use TinyHead, my RadioHead library for ATTiny85.

## Sleep  
The main function is to sleep for a set amount of minutes. This is not exact.  
Check the example for, uhm, an example.

## Sentry  
The secondary function is to wake on pin interrupt, I call this the sentry.  
Check the example for, uhm, an example.

## Note
There are examples in the `examples` directory.
Feel free to hit me up with questions or general feedbnack.
