// Sentry Pin Interrupt Example for the PowerNap library
// https://gitlab.com/thijsvanulden/PowerNap

#include <powernap.h>

#define LED 0    // Pin with LED
#define SENTRY 2 // Pin that wakes ATTiny on change

Napper napper;   // Initialize Napper

void setup() {
}

void loop() {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  napper.delay(200);  // Delay without using Timer
  digitalWrite(LED, HIGH);
  napper.delay(200);
  digitalWrite(LED, LOW);
  napper.delay(200);

  napper.sentry(SENTRY); // Go to sleep, wait on interrupt on SENTRY pin
}
