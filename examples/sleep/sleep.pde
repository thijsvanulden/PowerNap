// Sleep Example for the PowerNap library
// https://gitlab.com/thijsvanulden/PowerNap

#include <powernap.h>

#define LED 0

Napper napper; // Initialize Napper

void setup() {
  napper.setup(); // Optional watchdog cycle timer
}

void loop() {
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LOW);
  napper.delay(200);  // Delay without using Timer
  digitalWrite(LED, HIGH);
  napper.delay(200);
  digitalWrite(LED, LOW);
  napper.delay(200);

  napper.napminutes(1); // Sleep minutes, do not set cycle time in setup
}
