#######################################
# Syntax Coloring Map For PowerNap
#######################################

#######################################
# Datatypes (KEYWORD1)
#######################################

Napper	KEYWORD1

#######################################
# Methods and Functions (KEYWORD2)
#######################################

init    	KEYWORD2
setup		KEYWORD2
sleep		KEYWORD2
avrdelay	KEYWORD2
napminutes	KEYWORD2

#######################################
# Constants (LITERAL1)
#######################################