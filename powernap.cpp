#include <avr/interrupt.h>
#include <avr/sleep.h>
#include <util/atomic.h>
#include <util/delay.h>
#include "powernap.h"

// This interrupt is used only as a wakeup source.
EMPTY_INTERRUPT(WDT_vect);

/*  Setting up the watchdog timer.
 *  Default cycle of ~4s to accomodate napminutes()
 *  Cycles: 1=30ms, 2=60ms, 3=120ms, 4=250ms, 5=500ms, 6=1s, 7=2s, 8=4s, 9=8s
 *  Code 'found' on the web, subject to change
 */
void Napper::setup(uint8_t cycle) {
    uint8_t prescaler;         // prescaler bits in WDTCR
    if (cycle > 9) cycle = 9;  // largest supported value
    prescaler = cycle & 7;     // bits WDP2:WDP0 are right aligned in WDTCR
    if (cycle > 7) prescaler |= _BV(WDP3);

    MCUSR &= ~_BV(WDRF);  	  // clear this bit: it overrides WDE
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {   // cannot be interrupted
        WDTCR |= _BV(WDCE)    // start timed sequence
              |  _BV(WDE);
        WDTCR =  prescaler    // set new watchdog timeout value
              |  _BV(WDIE);   // enable watchdog interrupt
    }
}

/* Setting up the external interrupt.
*  By default on pin 0
*  Pull-up activated on triggerpin so pull low for action
*/
void Napper::sentry(uint8_t triggerpin) {
  PORTB |= _BV(triggerpin);                // Set pin to input with pull-up
  // DDRB = _BV(DDB3);                       // setting something to output?
  GIMSK |= _BV(PCIE);                     // Enable Pin Change Interrupts
  PCMSK |= _BV(triggerpin);               // Use PCINT4 as interrupt pin
  ADCSRA &= ~_BV(ADEN);                   // ADC off
  set_sleep_mode(SLEEP_MODE_PWR_DOWN);    // replaces above statement

  sleep_enable();
  sleep_bod_disable();                    // Sets the Sleep Enable bit in the MCUCR Register (SE BIT)
  sei();                                  // Enable interrupts
  sleep_cpu();                            // sleep
  cli();                                  // Disable interrupts

  PCMSK &= ~_BV(triggerpin);                  // Turn off PCINT4 as interrupt pin
  sleep_disable();                        // Clear SE bit
  ADCSRA |= _BV(ADEN);                    // ADC on

  // sei();                                  // Enable interrupts
}


/*  Here runs the sleeper
 *  Going to sleep and coming from sleep
 *  Some ADC magic for power consumption
 */
void Napper::sleep() {
    ADCSRA &= ~_BV(ADEN);
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);  // Add to settings, power down default
    sleep_mode();
    ADCSRA |= _BV(ADEN);
}

/*  AVR Delay to support use of timer0 for other things
 *  Added for support of RadioHead
 */
void Napper::delay(double ms) {
  _delay_ms(ms);                     // AVR Util function for sleeping
}

/*  Sleeping in minutes, approximatly
 *  Tested to be less than 10% inaccurate
 */
void Napper::napminutes(uint8_t minutes) {
  for (int c = 0; c < minutes; c++) { // How many minutes
    for (int i = 0; i < 15; i++) {    // Cycles of 15*4 sec, aka ~minutes
      sleep();                        // Start sleeping
    }
  }
}
