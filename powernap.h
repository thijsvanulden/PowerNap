#ifndef POWERNAP_H
#define POWERNAP_H

#include <stdint.h>

class Napper {
	public:
		Napper() {}
		void init(float initial);
		void setup(uint8_t cycle = 8);
		void sleep();
		void delay(double ms);
		void napminutes(uint8_t minutes);
		void sentry(uint8_t triggerpin = 0);
};

#endif	// defined(POWERNAP_H)
